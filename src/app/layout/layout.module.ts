import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
	MatFormFieldModule,
	MatIconModule,
	MatSelectModule,
} from '@angular/material';
import { FooterComponent } from './component/footer';
import { HeaderComponent } from './component/header';
import { SidebarComponent } from './component/sidebar';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './page/layout';

@NgModule({
	declarations: [
		LayoutComponent,
		HeaderComponent,
		SidebarComponent,
		FooterComponent,
	],
	imports: [
		CommonModule,
		LayoutRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		MatIconModule,
		MatFormFieldModule,
		MatSelectModule,
	],
})
export class LayoutModule {}
