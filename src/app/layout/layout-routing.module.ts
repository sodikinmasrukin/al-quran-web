import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './page/layout';

const routes: Routes = [
	{
		path: '',
		component: LayoutComponent,
		children: [
			{
				path: '',
				loadChildren: () =>
					import('../pages/homepage/homepage.module').then(
						m => m.HomepageModule
					),
			},
			{
				path: 'surah',
				loadChildren: () =>
					import('../pages/surah/surah.module').then(
						m => m.SurahModule
					),
			},
		],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class LayoutRoutingModule {}
