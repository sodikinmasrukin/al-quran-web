import { Component, Input, OnInit } from '@angular/core';
import { SurahVersesModel } from '../../shared/model';

@Component({
	selector: 'app-ayat',
	templateUrl: './ayat.component.html',
	styleUrls: ['./ayat.component.scss'],
})
export class AyatComponent implements OnInit {
	@Input()
	public ayat: SurahVersesModel;

	constructor() {}

	ngOnInit() {}
}
