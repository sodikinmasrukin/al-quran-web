import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SurahComponent } from './page/surah';

const routes: Routes = [
	{
		path: '',
		component: SurahComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class SurahRoutingModule {}
