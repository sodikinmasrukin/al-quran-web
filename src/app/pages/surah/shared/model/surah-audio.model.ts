export class SurahAudioModel {
	public index: number;
	public title: string;
	public link: string;

	public convert(dto: any): SurahAudioModel {
		this.index = 0;
		this.title = '';
		if (dto.url !== null) {
			this.link = dto.url;
		}

		return this;
	}
}
