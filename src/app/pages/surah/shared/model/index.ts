export * from './surah-audio.model';
export * from './surah-media.model';
export * from './surah-meta.model';
export * from './surah-translate.model';
export * from './surah-verses.model';
export * from './surah.model';
