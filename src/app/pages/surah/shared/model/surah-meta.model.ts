export class SurahMetaModel {
	public currentPage: number;
	public nextPage: number;
	public prevPage: number;
	public totalCount: number;
	public totalPages: number;

	public convert(dto: any): SurahMetaModel {
		if (dto.current_page !== null) {
			this.currentPage = dto.current_page;
		}
		if (dto.next_page !== null) {
			this.nextPage = dto.next_page;
		}
		if (dto.prev_page !== null) {
			this.prevPage = dto.prev_page;
		}
		if (dto.total_count !== null) {
			this.totalCount = dto.total_count;
		}
		if (dto.total_pages !== null) {
			this.totalPages = dto.total_pages;
		}

		return this;
	}
}
