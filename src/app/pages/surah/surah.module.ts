import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxAudioPlayerModule } from 'ngx-audio-player';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AyatComponent } from './component/ayat';
import { SurahComponent } from './page/surah';
import { SurahRoutingModule } from './surah-routing.module';

@NgModule({
	declarations: [SurahComponent, AyatComponent],
	imports: [
		CommonModule,
		SurahRoutingModule,
		InfiniteScrollModule,
		NgxAudioPlayerModule,
	],
})
export class SurahModule {}
