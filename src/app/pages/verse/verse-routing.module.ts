import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VerseComponent } from './page/verse';

const routes: Routes = [
	{
		path: '',
		component: VerseComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class VerseRoutingModule {}
