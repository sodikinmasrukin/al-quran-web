import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { VerseComponent } from './page/verse';
import { VerseRoutingModule } from './verse-routing.module';

@NgModule({
	declarations: [VerseComponent],
	imports: [CommonModule, VerseRoutingModule],
})
export class VerseModule {}
