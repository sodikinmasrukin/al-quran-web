import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ChapterCardComponent } from './component/chapter-card';
import { HomepageRoutingModule } from './homepage-routing.module';
import { HomepageComponent } from './page/homepage';

@NgModule({
	declarations: [HomepageComponent, ChapterCardComponent],
	imports: [CommonModule, HomepageRoutingModule],
})
export class HomepageModule {}
