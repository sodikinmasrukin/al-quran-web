export class TranslateModel {
	public languageName: string;
	public name: string;

	public convert(dto: any): TranslateModel {
		if (dto.language_name !== null) {
			this.languageName = dto.language_name;
		}
		if (dto.name !== null) {
			this.name = dto.name;
		}

		return this;
	}
}
