import { TranslateModel } from './translate.model';

export class ChapterModel {
	public id: number;
	public chapterNumber: number;
	public bismillahPre: boolean;
	public revelationOrder: number;
	public revelationPlace: string;
	public nameComplex: string;
	public nameArabic: string;
	public nameSimple: string;
	public versesCount: number;
	public pages: number[];
	public translatedName: TranslateModel;

	public convert(dto: any): ChapterModel {
		if (dto.id !== null) {
			this.id = dto.id;
		}
		if (dto.chapter_number !== null) {
			this.chapterNumber = dto.chapter_number;
		}
		if (dto.bismillah_pre !== null) {
			this.bismillahPre = dto.bismillah_pre;
		}
		if (dto.revelation_order !== null) {
			this.revelationOrder = dto.revelation_order;
		}
		if (dto.revelation_place !== null) {
			this.revelationPlace = dto.revelation_place;
		}
		if (dto.name_complex !== null) {
			this.nameComplex = dto.name_complex;
		}
		if (dto.name_arabic !== null) {
			this.nameArabic = dto.name_arabic;
		}
		if (dto.name_simple !== null) {
			this.nameSimple = dto.name_simple;
		}
		if (dto.verses_count !== null) {
			this.versesCount = dto.verses_count;
		}
		if (dto.pages !== null) {
			this.pages = dto.pages;
		}
		if (dto.translated_name !== null) {
			this.translatedName = new TranslateModel().convert(
				dto.translated_name
			);
		}

		return this;
	}
}
