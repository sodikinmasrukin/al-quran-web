import { environment } from '../../../../../environments/environment';

const chapterServicePath = 'chapters';

export const ChapterServicePathConst = `${environment.apiV3}/${chapterServicePath}`;
